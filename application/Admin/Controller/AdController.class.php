<?php
namespace Admin\Controller;

use Common\Controller\AdminbaseController;
use Common\Controller\WeixinController;

class AdController extends AdminbaseController
{
    protected $ad_model;

    public function _initialize()
    {
        parent::_initialize();
        $this->ad_model = D("Common/Ad");
    }

    // 后台广告列表
    public function index()
    {
        $ads = $this->ad_model->select();
        foreach ($ads as $k => $v) {
            $ads[$k]['status_info'] = $v['status'] == 1 ? '进行中' : ($v['status'] == 2 ? '已完成' : ($v['status'] == 3 ? '已暂停' : '未开始'));
            $ads[$k]['add_time']    = date('Y-m-d H:i', $v['add_time']);
            $ads[$k]['update_time'] = date('Y-m-d H:i', $v['update_time']);
        }
        $this->assign("ads", $ads);
        $this->display();
    }

    // 广告添加
    public function add()
    {
        $this->display();
    }

    // 广告添加提交
    public function add_post()
    {

        if (IS_POST) {
            if ($task = S('taskSendMessage')) {
                $this->error('上次推送还未完成，请等完成后再发送' . $task);
            }
            $post             = I('post.');
            $end_date         = I('post.end_date');
            $end_date         = $end_date ? '-' . $end_date . ' day' : 0;
            $post['end_time'] = $end_date ? strtotime($end_date) : 0;
            $start_time       = strtotime('-7 day') - 3600;
            $form_model       = M('UserFormid');
            $where            = 'add_time > ' . $start_time;
            if ($post['end_time'] > 0) {
                $where .= ' and add_time < ' . $post['end_time'];
            }
            $row               = $form_model->field('COUNT(DISTINCT user_id) AS num')
                ->where($where)->find();
            $post['total_num'] = (int)$row['num'];
            $post['status']    = 0;
            $post['add_time']  = time();
            if ($this->ad_model->create($post) !== false) {
                $id = $this->ad_model->add();
                if ($id !== false) {
                    S('taskSendMessage', $id, ['expire' => 86400]);
                    $this->success(L('ADD_SUCCESS'), U("ad/index"));
                } else {
                    $this->error(L('ADD_FAILED'));
                }
            } else {
                $this->error($this->ad_model->getError());
            }
        }
    }

    // 广告编辑
    public function edit()
    {
        $id = I("get.id", 0, 'intval');
        $ad = $this->ad_model->where(['ad_id' => $id])->find();
        $this->assign($ad);
        $this->display();
    }

    // 广告编辑提交
    public function edit_post()
    {
        if (IS_POST) {
            if ($this->ad_model->create() !== false) {
                if ($this->ad_model->save() !== false) {
                    $this->success("保存成功！", U("ad/index"));
                } else {
                    $this->error("保存失败！");
                }
            } else {
                $this->error($this->ad_model->getError());
            }
        }
    }

    // 广告删除
    public function delete()
    {
        $id = I("get.id", 0, "intval");
        if ($this->ad_model->delete($id) !== false) {
            $this->success("删除成功！");
        } else {
            $this->error("删除失败！");
        }
    }

    // 广告显示/隐藏
    public function toggle()
    {
        if (!empty($_POST['ids']) && isset($_GET["display"])) {
            $ids = I('post.ids/a');
            if ($this->ad_model->where(['ad_id' => ['in', $ids]])->save(['status' => 1]) !== false) {
                $this->success("显示成功！");
            } else {
                $this->error("显示失败！");
            }
        }

        if (isset($_POST['ids']) && isset($_GET["hide"])) {
            $ids = I('post.ids/a');
            if ($this->ad_model->where(['ad_id' => ['in', $ids]])->save(['status' => 0]) !== false) {
                $this->success("隐藏成功！");
            } else {
                $this->error("隐藏失败！");
            }
        }
    }

    // 广告添加
    public function get_user_num()
    {
        $end_date         = I('post.end_date');
        $end_date         = $end_date ? '-' . $end_date . ' day' : 0;
        $post['end_time'] = $end_date ? strtotime($end_date) : 0;
        $start_time       = strtotime('-7 day') - 3600;
        $form_model       = M('UserFormid');
        $where            = 'add_time > ' . $start_time;
        if ($post['end_time'] > 0) {
            $where .= ' and add_time <' . $post['end_time'];
        }
        $row = $form_model->field('COUNT(DISTINCT user_id) AS num')
            ->where($where)->find();

        $this->ajaxReturn(['status' => 20000, 'code' => 20000, 'total_num' => (int)$row['num']]);
    }

    // 广告暂停
    public function task_stop()
    {
        $id = I("id", 0, "intval");
        $ret = $this->ad_model->where(['ad_id'=>$id])->save(['status'=>3]);
        if ($ret) {
            $this->success("操作成功！");
        } else {
            $this->error("操作失败！");
        }
    }

    // 广告开启
    public function task_start()
    {
        $id = I("id", 0, "intval");
        $ret = $this->ad_model->where(['ad_id'=>$id])->save(['status'=>1]);
        if ($ret) {
            $this->success("操作成功！");
        } else {
            $this->error("操作失败！");
        }
    }

    // 广告停止
    public function task_finish()
    {
        $id = I("id", 0, "intval");
        $ret = $this->ad_model->where(['ad_id'=>$id])->save(['status'=>2]);
        if ($ret) {
            $this->success("操作成功！");
        } else {
            $this->error("操作失败！");
        }
    }
}