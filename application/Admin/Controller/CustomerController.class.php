<?php
namespace Admin\Controller;

use Common\Controller\AdminbaseController;

class CustomerController extends AdminbaseController{

	protected $users_model;

	public function _initialize() {
		parent::_initialize();
		$this->users_model = D("WxUser");
	}

	// 管理员列表
	public function index()
    {
        /**搜索条件**/
        $keywork = trim(I('request.keywork'));
        $is_bark = I('is_bark');
        $orderby = I('orderby');
        $where = [];
        if ($keywork) {
            if(is_numeric($keywork)){
                $where['id'] = $keywork;
            }else{
                $where['openid'] = $keywork;
                $where['nick_name'] = array('like', "%$keywork%");
                $where['_logic'] = 'OR';
            }
        }
        $andWhere = [];
        if ($is_bark) {
            if($is_bark == 1){
                $andWhere['is_bark'] = 1;
            }else{
                $andWhere['is_bark'] = 0;
            }
        }
        $order = "id DESC";
        if($orderby == 1){
            $order = "rank_num DESC";
        }elseif($orderby == 2){
            $order = "amount DESC";
        }elseif($orderby == 3){
            $order = "update_time DESC";
        }
        $count = $this->users_model->where($where)->where($andWhere)->count();
        $page = $this->page($count, 20);

        $users = $this->users_model
            ->where($where)
            ->where($andWhere)
            ->order($order)
            ->limit($page->firstRow, $page->listRows)
            ->select();
        $pay_list = [];
        $receive_list = [];
        $wd_list = [];
        $userIds = $users ? array_column($users,'id') : [];
        if($userIds){
            $where = 'user_id in ('.implode(',',$userIds).')';
            $pay_count = M('enve')
                ->field('user_id,COUNT(id) as pay_num,SUM(show_amount) as pay_amount')
                ->where($where)
                ->where(['pay_status'=>"ok"])
                ->group("user_id")
                ->select();
            $receive_count = M('enve_receive')
                ->field('user_id,COUNT(id) as receive_num,SUM(receive_amount) as receive_amount')
                ->where($where)
                ->group("user_id")
                ->select();
            $wd_count = M('withdrawals')
                ->field('user_id,COUNT(id) as wd_num,SUM(amount) as wd_amount')
                ->where($where)
                ->where(['status'=>"SUCCESS"])
                ->group("user_id")
                ->select();
            foreach ($pay_count as $v){
                $pay_list[$v['user_id']] = $v;
            }
            foreach ($receive_count as $v){
                $receive_list[$v['user_id']] = $v;
            }
            foreach ($wd_count as $v){
                $wd_list[$v['user_id']] = $v;
            }
        }

        foreach ($users as &$v){
            $v['bark_info'] = $v['is_bark']==1 ? '是':'否';
            $v['sex'] = $v['sex']==1 ? '男':'女';
            $v['add_time'] = date('Y-m-d H:i:s',$v['add_time']);
            $v['update_time'] = date('Y-m-d H:i:s',$v['update_time']);
            $v['update_time'] = date('Y-m-d H:i:s',$v['update_time']);
            if(isset($pay_list[$v['id']])){
                $v['pay_num'] = (int)$pay_list[$v['id']]['pay_num'];
                $v['pay_amount'] = number_format($pay_list[$v['id']]['pay_amount'],2);
            }else{
                $v['pay_num'] = 0;
                $v['pay_amount'] = 0;
            }
            if(isset($receive_list[$v['id']])){
                $v['receive_num'] = (int)$receive_list[$v['id']]['receive_num'];
                $v['receive_amount'] = number_format($receive_list[$v['id']]['receive_amount'],2);
            }else{
                $v['receive_num'] = 0;
                $v['receive_amount'] = 0;
            }
            if(isset($wd_list[$v['id']])){
                $v['wd_num'] = (int)$wd_list[$v['id']]['wd_num'];
                $v['wd_amount'] = number_format($wd_list[$v['id']]['wd_amount'],2);
            }else{
                $v['wd_num'] = 0;
                $v['wd_amount'] = 0;
            }
            //$v['amount_status'] = $v['amount'] == ($v['receive_amount']-$v['pay_amount']-$v['wd_amount']) ? 1 : 0;
        }

        $this->assign("page", $page->show('Admin'));
        $this->assign("users", $users);
        $this->display();
    }
    private function setBarker(){
        $cacheKey = 'cachegetBarker';
        $barkers = [];
        $user = M("WxUser")->where(['is_bark'=>1])->select();

        if($user){
            foreach ($user as $v){
                $barkers[] = $v['id'];
            }
        }
        //设置缓存
        S($cacheKey, $barkers, ['expire' => 2592000]);

        return $barkers;
    }
    // 刷单
    public function flag_bark(){
        $id = I('user_id',0,'intval');
        $ret = M("WxUser")->where(array("id"=>$id))->save(['is_bark'=>1]);
        if ($ret) {
            $this->setBarker();
            $this -> ajaxReturn(['msg' => '操作成功']);
        } else {
            $this -> ajaxReturn(['msg' => '操作失败']);
        }
    }
    // 解除刷单
    public function recover_bark(){
        $id = I('user_id',0,'intval');
        $ret = M("WxUser")->where(array("id"=>$id))->save(['is_bark'=>0]);
        if ($ret) {
            $this->setBarker();
            $this -> ajaxReturn(['msg' => '操作成功']);
        } else {
            $this -> ajaxReturn(['msg' => '操作失败']);
        }
    }
// 禁用
    public function forbid(){
        $id = I('user_id',0,'intval');
        $ret = M("WxUser")->where(array("id"=>$id))->save(['status'=>1]);

        if ($ret) {
            $this -> ajaxReturn(['msg' => '操作成功']);
        } else {
            $this -> ajaxReturn(['msg' => '操作失败']);
        }
    }
    // 启用
    public function recover(){
        $id = I('user_id',0,'intval');
        $ret = M("WxUser")->where(array("id"=>$id))->save(['status'=>0]);

        if ($ret) {
            $this -> ajaxReturn(['msg' => '操作成功']);
        } else {
            $this -> ajaxReturn(['msg' => '操作失败']);
        }
    }

}