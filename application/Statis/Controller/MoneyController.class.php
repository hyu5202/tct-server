<?php
/**
 * 资金统计
 * author hhz 2017.10.10
 */
namespace Statis\Controller;

use Common\Controller\AdminbaseController;
use Common\Controller\WeixinController;
use Think\Controller;

class MoneyController extends AdminbaseController
{
    //用户显示界面
    public function index()
    {
        $this->display();
    }

    /**
     * 统计佣金总额
     * @return bool
     * time 2017.10.17
     */
    public function get_sum()
    {
        $begin_date = strtotime(I('begin_date'));
        $end_date   = strtotime(I('end_date'));
        if (!$end_date && !$begin_date) {
            $this->ajaxReturn(['code' => 40000, 'msg' => '时间不正确']);
        }
        if ($end_date - $begin_date < 0) {
            $this->ajaxReturn(['code' => 40000, 'msg' => '开始时间不能大于结束时间']);
        }
        $where = 'add_time > ' . $begin_date . ' and add_time <= ' . $end_date;
        //$where = [];
        $pay_model  = M('PayLog');
        $enve_model = M('Enve');
        $wd_model   = M('Withdrawals');
        $user_model = M('WxUser');
        $pay_where  = [
            'type' => 1,
        ];
        //支付统计
        $pay_log = $pay_model->field(['count(DISTINCT user_id) as total_user','pay_type', 'count(id) as total_num', ' sum(amount) as total_amount'])
            ->where($pay_where)
            ->where($where)
            ->group('pay_type')->select();
        //未支付统计
        $uppay_where  = $where.' and pay_status = ""';
        $unpay_count= $enve_model->field(['count(id) as total_num', ' sum(show_amount) as total_amount'])
            ->where($uppay_where)->find();
        $pay_count    = [
            'total_num'      => 0,
            'total_amount'   => 0,
            'wxpay_amount'   => 0,
            'balance_amount' => 0,
            'unpay_amount'   => number_format($unpay_count['total_amount'],2),
            'unpay_num'   => (int)$unpay_count['total_num'],
            'pay_user'   => 0,
        ];
        foreach ($pay_log as $v) {
            if ($v['pay_type'] == 1) {
                $pay_count['wxpay_amount'] = $v['total_amount'];
            } else {
                $pay_count['balance_amount'] = $v['total_amount'];
            }
            $pay_count['total_num']    += $v['total_num'];
            $pay_count['total_amount'] += $v['total_amount'];
            $pay_count['pay_user'] += $v['total_user'];
        }
        $pay_count['total_amount'] = number_format($pay_count['total_amount'], 2);
        //提现统计
        $wd_log   = $wd_model->field(['count(id) as total_num', ' sum(amount) as total_amount', ' sum(true_amount) as total_true_amount'])
            ->where($where)
            ->where(['status'=>'SUCCESS'])
            ->find();
        $wd_count = [
            'total_num'     => 0,
            'total_amount'  => 0,
            'true_amount'   => 0,
            'profit_amount' => 0,
        ];
        if ($wd_log) {
            $wd_count                  = [
                'total_num'     => $wd_log['total_num'],
                'total_amount'  => $wd_log['total_amount'] ?: '0',
                'true_amount'   => $wd_log['total_true_amount'] ?: '0',
                'profit_amount' => $wd_log['total_amount'] - $wd_log['total_true_amount'],
            ];
            $wd_count['profit_amount'] = number_format($wd_count['profit_amount'], 2);
        }
        //用户余额
        $user_amount = $user_model->Sum('amount');
        $data        = [
            'pay_count'    => $pay_count,
            'wd_count'     => $wd_count,
            'user_amount'  => $user_amount,
            'sum_amount'   => $pay_count['total_amount'],
            'sum_profit'   => $wd_count['profit_amount'],
            'nopay_amount' => $pay_count['unpay_amount'],
        ];

        $data = ['status' => 20000, 'code' => 20000, 'data' => $data];
        $this->ajaxReturn($data);
    }
}
